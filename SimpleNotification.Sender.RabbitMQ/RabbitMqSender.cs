﻿using System;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using SimpleNotification.Core;

namespace SimpleNotification.Sender.RabbitMQ
{
    public class RabbitMqSender: INotificationSender
    {
        private readonly ILogger<RabbitMqSender> _logger;
        private ConnectionFactory _factory;
        private readonly RabbitMqSettings _settings;

        public RabbitMqSender(RabbitMqSettings settings,ILogger<RabbitMqSender> logger)
        {
            _settings = settings;
            _logger = logger;
            InitChannel();
        }
        
        public void SendNotification(NotificationTyp type, Guid userId, string subject, string message)
        {
            if (type == null) throw new ArgumentNullException(nameof(type));
            var notification = new Core.SimpleNotification()
            {
                Message = message,
                Subject = subject,
                NotificationType = type,
                UserId = userId
            };
            SendNotification(notification);
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void SendNotification(Core.SimpleNotification notification)
        {
            if (notification == null) throw new ArgumentNullException(nameof(notification));
            _logger.LogInformation("Send Notification of type " +notification);
            var jsonNotification = JsonConvert.SerializeObject(notification);
            var rawNotification = Encoding.UTF8.GetBytes(jsonNotification);
            var channel = GetChannel();
            channel.BasicPublish(_settings.Exchange,_settings.RoutingKey,null,rawNotification);
        }

        private void InitChannel()
        {
            _logger.LogDebug($"Init RabbitMQ Connection Factory");
            _factory = new ConnectionFactory()
            {
                Password = _settings.Password,
                Port = _settings.Port,
                HostName = _settings.HostName,
                UserName = _settings.UserName,
            };
        }

        private IModel GetChannel()
        {
            //TODO Use Pool For that
            var channel = GetConnection().CreateModel();
            return channel;
        }
        
        private IConnection GetConnection()
        {
            //TODO Create Connection Pool
            return _factory.CreateConnection();
        }
    }
}