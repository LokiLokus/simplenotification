using System;
using RabbitMQ.Client;

namespace SimpleNotification.Sender.RabbitMQ
{
    public class RabbitMqSettings:SimpleNotificationSettings
    {
        public string UserName { get; set; }
        public string HostName { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }

        public string Exchange { get; set; } = "SimpleNotification";
        public string RoutingKey { get; set; } = "default.simplenotification.push";
    }
}