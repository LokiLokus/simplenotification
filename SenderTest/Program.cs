﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using RabbitMQ.Client;
using SimpleNotification.Core;
using SimpleNotification.Sender.RabbitMQ;

namespace SenderTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var sender = new RabbitMqSender(new RabbitMqSettings()
            {
                Exchange = "Message",
                Password = "guest",
                Port = 5672,
                HostName = "localhost",
                UserName = "guest"
            },new Logger<RabbitMqSender>(new NullLoggerFactory()));
            
            sender.SendNotification(new NotificationTyp("Test"),Guid.NewGuid(), "First Notification","This is your first Notification");
        }
    }
}