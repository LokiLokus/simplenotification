using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using SimpleNotification.Consumer.Exceptions;
using SimpleNotification.Consumer.User;

namespace SimpleNotification.Consumer
{
    public class ConsumerService:IDisposable
    {
        private readonly ISettingsGetter _settingsGetter;
        private readonly Dictionary<SendTyp, INotifier> _typeNotificationServices;
        private readonly IConsumer _consumer;
        private readonly ILogger<ConsumerService> _logger;

        public ConsumerService(ISettingsGetter settingsGetter,IConsumer consumer, Dictionary<SendTyp, INotifier> typeNotificationServices, ILogger<ConsumerService> logger) 
        {
            _settingsGetter = settingsGetter ?? throw new ArgumentNullException(nameof(settingsGetter));
            _typeNotificationServices = typeNotificationServices ?? throw new ArgumentNullException(nameof(typeNotificationServices));
            _consumer = consumer ?? throw new ArgumentNullException(nameof(consumer));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            InitConnectionAndEvent();
        }

        private void InitConnectionAndEvent()
        {
            try
            {
                _consumer.InitConnection();
                _consumer.OnNewNotification += ConsumerOnOnNewNotification;
            }
            catch (Exception e)
            {
                _logger.LogCritical($"Connection failed");
                throw;
            }
        }

        private void ConsumerOnOnNewNotification(Core.SimpleNotification notification, Guid requestId)
        {
            _logger.LogInformation($"Send Notification with Type {notification.NotificationType} to User with Id {notification.UserId}");
            UserSetting userSetting = _settingsGetter.GetUserSetting(notification.UserId);
            if (userSetting == null)
            {
                _logger.LogError($"User with Id {notification.UserId} not found");
                throw new UserSettingNotFoundException("User with Id: " + notification.UserId + " not found");
            }

            if (!userSetting.UserSettings.ContainsKey(notification.NotificationType))
            {
                _logger.LogError($"User has no Setting for Notification type \"{notification?.NotificationType}\". Maybe you should Init Default Settings for the Users");
                throw new NotificationTypeInUserSettingNotFoundException($"User {notification?.UserId} has no Setting for Notification type \"{notification?.NotificationType}\". Maybe you should Init Default Settings for the Users");
            }

            if (!userSetting.UserSettings[notification.NotificationType].SendNotification)
            {
                _logger.LogDebug($"User {notification.UserId} disabled SendNotification for Type {notification.NotificationType}");
                return;
            }
            
            foreach (var sendTyp in userSetting.UserSettings[notification.NotificationType].SendTyps)
            {
                if (!_typeNotificationServices.ContainsKey(sendTyp.Key))
                {
                    //Not throw an Exception here => Even when one Send Typ is Missing, The User gets Notifications with other Send typ
                    _logger.LogError($"SendType {sendTyp.Key.Typ} not Found in Global Settings");
                }
                else
                {
                    _typeNotificationServices[sendTyp.Key].SendNotification(userSetting,notification);
                    _consumer.Acknowledge(requestId);
                }
            }
        }

        public void Dispose()
        {
            _consumer?.Dispose();
        }
    }
}