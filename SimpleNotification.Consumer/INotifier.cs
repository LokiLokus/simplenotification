using SimpleNotification.Consumer.User;

namespace SimpleNotification.Consumer
{
    public interface INotifier
    {
        void SendNotification(UserSetting userSetting, Core.SimpleNotification simpleNotification);
    }
}