using System;

namespace SimpleNotification.Consumer.User
{
    public interface ISettingsGetter {
        UserSetting GetUserSetting(Guid userId);
    }
}