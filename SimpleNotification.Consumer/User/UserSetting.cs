﻿using System.Collections.Generic;
using SimpleNotification.Core;

namespace SimpleNotification.Consumer.User {
	public class UserSetting {
		public GlobalUserSettings GlobalUserSettings { get; set; }

		/// <summary>
		/// NotificationTyp <-> InternUserSetting
		/// </summary>
		public Dictionary<NotificationTyp, InternUserSetting> UserSettings { get; set; } =
			new Dictionary<NotificationTyp, InternUserSetting>();
	}

	public class InternUserSetting {
		public bool SendNotification { get; set; }
		/// <summary>
		/// Send Typ <-> SendConfig
		/// </summary>
		public Dictionary<SendTyp,SendConfig> SendTyps { get; set; } = new Dictionary<SendTyp, SendConfig>();
	}

	/// <summary>
	/// Should the Notification sended or Not
	/// Maybe extended in further Versions
	/// </summary>
	public enum SendConfig {
		DontUse,SendInstant
	}
}