using System;

namespace SimpleNotification.Consumer.User {
	/// <summary>
	/// User Settings e.g. Email Address, Name & Co.,
	/// </summary>
	public abstract class GlobalUserSettings {
		public Guid UserId { get; set; }
	}
}