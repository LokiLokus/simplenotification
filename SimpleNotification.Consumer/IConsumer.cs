using System;

namespace SimpleNotification.Consumer
{
    public interface IConsumer:IDisposable
    {
        void InitConnection();
        event NewNotification OnNewNotification;
        void Acknowledge(Guid requestId);
    }
    public delegate void NewNotification(Core.SimpleNotification notification,Guid requestId);
}