using System;

namespace SimpleNotification.Consumer.Exceptions
{
    public class UserSettingNotFoundException: Exception
    {
        public UserSettingNotFoundException(string notificationUserId): base(notificationUserId)
        {
            
        }
    }
}