using System;

namespace SimpleNotification.Consumer.Exceptions
{
    public class NotificationTypeInUserSettingNotFoundException: Exception
    {
        public NotificationTypeInUserSettingNotFoundException(string s):base(s)
        {
        }
    }
}