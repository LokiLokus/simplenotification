using System;

namespace SimpleNotification.Consumer.Exceptions
{
    public class NotificationTypeHasNoMatchingSenderException:Exception
    {
        public NotificationTypeHasNoMatchingSenderException(string s):base(s)
        {
            
        }
    }
}