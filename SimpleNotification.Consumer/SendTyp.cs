using System.Diagnostics;

namespace SimpleNotification.Consumer
{
    [DebuggerDisplay("SendTyp = {Typ}")]
    public class SendTyp
    {
        public readonly string Typ;

        public SendTyp(string typ)
        {
            Typ = typ;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != typeof(SendTyp))
                return false;
            var sendObj = (SendTyp) obj;
            return sendObj.Typ == this.Typ;
        }

        protected bool Equals(SendTyp other)
        {
            return Typ == other.Typ;
        }

        public override int GetHashCode()
        {
            return (Typ != null ? Typ.GetHashCode() : 0);
        }
    }
}