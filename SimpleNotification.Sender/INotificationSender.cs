using System;
using SimpleNotification.Core;

namespace SimpleNotification.Sender
{
    public interface INotificationSender
    {
        void SendNotification(NotificationTyp notificationType, Guid userId, string subject, string message);
        void SendNotification(Core.SimpleNotification notification);
    }
}