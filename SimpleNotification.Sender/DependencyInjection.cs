using System;
using Microsoft.Extensions.DependencyInjection;
using SimpleNotification.Core;

namespace SimpleNotification.Sender
{
    public static class DependencyInjection
    {
        private static IServiceCollection AddSimpleNotificationSender<T>(IServiceCollection services, SimpleNotificationSettings simpleSettings) where T: class,INotificationSender
        {
            if (simpleSettings == null) throw new ArgumentNullException(nameof(simpleSettings));
            SimpleNotificationSettings._settings = simpleSettings;
            services.AddSingleton<INotificationSender, T>();
            return services;
        }
    }
}