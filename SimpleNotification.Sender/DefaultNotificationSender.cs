using System;
using Microsoft.Extensions.Logging;
using SimpleNotification.Core;

namespace SimpleNotification.Sender
{
    public class DefaultNotificationSender:INotificationSender
    {
        private readonly ILogger<DefaultNotificationSender> _Logger;

        public DefaultNotificationSender(ILogger<DefaultNotificationSender> logger)
        {
            _Logger = logger;
        }
        
        public void SendNotification(NotificationTyp notificationType, Guid userId, string subject, string message)
        {
            SendNotification(new Core.SimpleNotification()
            {
                Message = message,
                Subject = subject,
                NotificationType = notificationType,
                UserId = userId
            });
        }

        public void SendNotification(Core.SimpleNotification notification)
        {
            if (_Logger == null)
            {
                Console.WriteLine($"NotificationTyp: {notification.NotificationType.Typ} UserId: {notification.UserId} Subject: {notification.Subject} Message: {notification.Message}");
            }
            else
            {
                _Logger.LogDebug($"NotificationTyp: {notification.NotificationType.Typ} UserId: {notification.UserId} Subject: {notification.Subject} Message: {notification.Message}");
            }
        }
    }
}