using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace SimpleNotification.Consumer.RabbitMQ
{
    public class RabbitMqConsumer: IConsumer
    {
        private readonly ILogger<RabbitMqConsumer> _logger;
        private ConnectionFactory _factory;
        private readonly RabbitMQSettings _settings;
        private readonly Dictionary<Guid, ulong> _toAcknowledgeRequests = new Dictionary<Guid, ulong>();
        public event NewNotification OnNewNotification;
        private IModel _channel;
        

        public RabbitMqConsumer(RabbitMQSettings settings, Logger<RabbitMqConsumer> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }
        
        public void InitConnection()
        {
            _logger.LogDebug($"Init RabbitMQ Connection Factory");
            _factory = new ConnectionFactory()
            {
                Password = _settings.Password,
                Port = _settings.Port,
                HostName = _settings.HostName,
                UserName = _settings.UserName,
            };
            _logger.LogDebug($"Init RabbitMQ Events");
            var channel = GetChannel();
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender, args) =>
            {
                _logger.LogInformation($"Received new Notification");
                var body = args.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                try
                {
                    var notification = JsonConvert.DeserializeObject<Core.SimpleNotification>(message);
                    _logger.LogInformation($"Invoke new Notification");
                    var requestId = Guid.NewGuid();
                    _toAcknowledgeRequests[requestId] = args.DeliveryTag;
                    OnNewNotification?.Invoke(notification,requestId);
                }
                catch (Exception e)
                {
                    _logger.LogError($"Cannot Deserialize {message}");
                }
            };
            
            channel.BasicConsume(_settings.Queue, autoAck: false, consumer);
        }
        
        public void Acknowledge(Guid requestId)
        {
            if (_toAcknowledgeRequests.ContainsKey(requestId))
            {
                GetChannel().BasicAck(_toAcknowledgeRequests[requestId],false);
            }
            else
            {
                _logger.LogError($"Can not Acknowledge Request with Id {requestId} cause not in ToAcknowledge Cache");
            }
        }
        
        public void Dispose()
        {
            _logger.LogInformation($"Shutting Down RabbitMQ Consumer");
        }

        private IModel GetChannel()
        {
            if(_channel == null) _channel = GetConnection().CreateModel();
            return _channel;
        }
        
        private IConnection GetConnection()
        {
            //TODO Create Connection Pool
            return _factory.CreateConnection();
        }
    }
}