﻿using System;

namespace SimpleNotification.Consumer.RabbitMQ
{
    public class RabbitMQSettings
    {
        public string UserName { get; set; }
        public string HostName { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }

        public string Queue { get; set; } = "SimpleNotification";
        public string RoutingKey { get; set; } = "default.simplenotification.push";
    }
}