using System.Diagnostics;

namespace SimpleNotification.Core
{
    [DebuggerDisplay("NotificationTyp = {Typ}")]
    public class NotificationTyp
    {
        
        public readonly string Typ;

        public NotificationTyp(string typ)
        {
            Typ = typ;
        }
        
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != typeof(NotificationTyp))
                return false;
            var sendObj = (NotificationTyp) obj;
            return sendObj.Typ == this.Typ;
        }

        protected bool Equals(NotificationTyp other)
        {
            return Typ == other.Typ;
        }

        public override int GetHashCode()
        {
            return (Typ != null ? Typ.GetHashCode() : 0);
        }
    }
}