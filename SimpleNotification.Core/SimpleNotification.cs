using System;

namespace SimpleNotification.Core
{
    public class SimpleNotification
    {
        public NotificationTyp NotificationType { get; set; }
        public Guid UserId { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}