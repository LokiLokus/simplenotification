using SimpleNotification.Consumer.User;

namespace ConsumerTest
{
    public class TestGlobalUserSetting:GlobalUserSettings
    {
        public string Name { get; set; }
    }
}