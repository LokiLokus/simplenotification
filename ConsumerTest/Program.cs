﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SimpleNotification.Consumer;
using SimpleNotification.Consumer.RabbitMQ;
using SimpleNotification.Notifier.Console;

namespace ConsumerTest
{
    class Program
    {
        static void Main(string[] args)
        {

            RabbitMqConsumer cons = new RabbitMqConsumer(new RabbitMQSettings()
            {
                Queue = "Message",
                Password = "guest",
                Port = 5672,
                HostName = "localhost",
                UserName = "guest",
            }, new Logger<RabbitMqConsumer>(new NullLoggerFactory()));

            var sendTyps = new Dictionary<SendTyp, INotifier>();
            sendTyps[new SendTyp("Console")] = new ConsoleNotifier();
            ConsumerService service = new ConsumerService(new UserSettingsTest(), cons, sendTyps,
                new Logger<ConsumerService>(new NullLoggerFactory()));
            cons.InitConnection();
            Console.ReadKey();
        }
    }
}