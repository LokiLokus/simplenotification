using System;
using System.Collections.Generic;
using SimpleNotification.Consumer;
using SimpleNotification.Consumer.User;
using SimpleNotification.Core;

namespace ConsumerTest
{
    public class UserSettingsTest :ISettingsGetter
    {
        public UserSetting GetUserSetting(Guid userId)
        {
            return new UserSetting()
            {
                GlobalUserSettings = new TestGlobalUserSetting()
                {
                    UserId = userId,
                    Name = "TestUser"
                },
                UserSettings = new Dictionary<NotificationTyp, InternUserSetting>()
                {
                    {
                        new NotificationTyp("Test"), new InternUserSetting()
                        {
                            SendNotification = true,
                            SendTyps = new Dictionary<SendTyp, SendConfig>()
                            {
                                {
                                    new SendTyp("Console"), SendConfig.SendInstant
                                }
                            }
                        }
                    }
                }
            };
        }
    }
}