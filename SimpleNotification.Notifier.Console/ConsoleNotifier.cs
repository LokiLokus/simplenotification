﻿using System;
using SimpleNotification.Consumer;
using SimpleNotification.Consumer.User;

namespace SimpleNotification.Notifier.Console
{
    public class ConsoleNotifier:INotifier
    {
        public void SendNotification(UserSetting userSetting, SimpleNotification.Core.SimpleNotification simpleNotification)
        {
            System.Console.WriteLine("From: " + userSetting.GlobalUserSettings.UserId);
            System.Console.WriteLine("Subject:");
            System.Console.WriteLine(simpleNotification.Subject);
            System.Console.WriteLine("Message:");
            System.Console.WriteLine(simpleNotification.Message);
        }
    }
}