# Simple Notification
SimpleNotification is a simple solution for handling Email, Push-Notifications and other One-Way Communications in your Application.

## How does it work?
SimpleNotification (SN) is splitted in two Parts, Consumer and Sender.
The Sender is the Microservice which should e.g. send an Email to an User. We will call it Herbert (such a nice name ;).
Herbert uses the ````INotificationSender```` Interface to send the required Notification data as an ````SimpleNotification```` Object to the Consumer. One Sender Implementation is e.g. the RabbitMQSender.

The ````SimpleNotification```` Object contains basically four Properties.
With the ````NotificationTyp````  the Application defines which kind of notification has to be sended.
An OAuth2 Microservice has e.g. four NotificationTypes like ````Register,Logout,LoginFromUnknownLocation,NewApplicationWantsGrants````. The User can define if and how the Notifications should be sended to him. In your example he can define to only get the ````LoginFromUnknownLocation```` NotificationTyp, but with the ````SendTyps```` Slack AND Email.

  ````
    public NotificationTyp NotificationType { get; set; }  
    public Guid UserId { get; set; }  
    public string Subject { get; set; }  
    public string Message { get; set; }
````

The Consumer receives the ````SimpleNotification```` Object and loads based on the given UserId the corresponding ````UserSetting````.
With this Settings the ````ConsumerService```` decides with which ````INotifier```` the Notification should be shipped.


## Examples
Consumer example:
https://gitlab.com/LokiLokus/simplenotification/-/tree/master/ConsumerTest
Sender example:
https://gitlab.com/LokiLokus/simplenotification/-/tree/master/SenderTest

## Flowdiagramm
A short diagram how the workflow works.
![Flow](https://gitlab.com/LokiLokus/simplenotification/-/raw/master/Ablauf%20NotificationSerivce.png)

